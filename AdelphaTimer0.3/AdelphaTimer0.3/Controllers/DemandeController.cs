﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdelphaTimer0._3.Models;

namespace AdelphaTimer0._3.Controllers
{
    public class DemandeController : Controller
    {
        private adelphaTimerEntities db = new adelphaTimerEntities();

        // GET: Demande
        public ActionResult Index()
        {
            var stories = db.Stories.Include(s => s.Categories).Include(s => s.Projects).Include(s => s.Ressources).Include(s => s.Sprints);
            return View(stories.ToList());
        }

        public ActionResult _Index()
        {
            var demande = db.Stories.ToList();
            return PartialView("_Index", demande);
        }

        // GET: Demande/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            return View(stories);
        }

        // GET: Demande/Create
        public ActionResult Create()
        {
            ViewBag.Id_cat_st = new SelectList(db.Categories, "Id_categ", "Desc_categ");
            ViewBag.Id_p_st = new SelectList(db.Projects, "Id_proj", "Nom_proj");
            ViewBag.Id_res_st = new SelectList(db.Ressources, "Id_res", "Nom_res");
            ViewBag.Id_sp_st = new SelectList(db.Sprints, "Id_sprint", "Etat_sprint");
            return View();
        }

        // POST: Demande/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_st,Nom_st,Id_res_st,Importance_st,Estim_init_st,Demontrer_st,Note,Id_cat_st,Demande_par_st,Demande_le_st,Id_p_st,Id_sp_st,Actif_st")] Stories stories)
        {
            if (ModelState.IsValid)
            {
                db.Stories.Add(stories);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_cat_st = new SelectList(db.Categories, "Id_categ", "Desc_categ", stories.Id_cat_st);
            ViewBag.Id_p_st = new SelectList(db.Projects, "Id_proj", "Nom_proj", stories.Id_p_st);
            ViewBag.Id_res_st = new SelectList(db.Ressources, "Id_res", "Nom_res", stories.Id_res_st);
            ViewBag.Id_sp_st = new SelectList(db.Sprints, "Id_sprint", "Etat_sprint", stories.Id_sp_st);
            return View(stories);
        }

        // GET: Demande/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_cat_st = new SelectList(db.Categories, "Id_categ", "Desc_categ", stories.Id_cat_st);
            ViewBag.Id_p_st = new SelectList(db.Projects, "Id_proj", "Nom_proj", stories.Id_p_st);
            ViewBag.Id_res_st = new SelectList(db.Ressources, "Id_res", "Nom_res", stories.Id_res_st);
            ViewBag.Id_sp_st = new SelectList(db.Sprints, "Id_sprint", "Etat_sprint", stories.Id_sp_st);
            return View(stories);
        }

        // POST: Demande/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_st,Nom_st,Id_res_st,Importance_st,Estim_init_st,Demontrer_st,Note,Id_cat_st,Demande_par_st,Demande_le_st,Id_p_st,Id_sp_st,Actif_st")] Stories stories)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stories).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_cat_st = new SelectList(db.Categories, "Id_categ", "Desc_categ", stories.Id_cat_st);
            ViewBag.Id_p_st = new SelectList(db.Projects, "Id_proj", "Nom_proj", stories.Id_p_st);
            ViewBag.Id_res_st = new SelectList(db.Ressources, "Id_res", "Nom_res", stories.Id_res_st);
            ViewBag.Id_sp_st = new SelectList(db.Sprints, "Id_sprint", "Etat_sprint", stories.Id_sp_st);
            return View(stories);
        }

        // GET: Demande/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stories stories = db.Stories.Find(id);
            if (stories == null)
            {
                return HttpNotFound();
            }
            return View(stories);
        }

        // POST: Demande/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Stories stories = db.Stories.Find(id);
            db.Stories.Remove(stories);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
