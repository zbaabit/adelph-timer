﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdelphaTimer0._3.Models;

namespace AdelphaTimer0._3.Controllers
{
    public class TachesController : Controller
    {
        private adelphaTimerEntities db = new adelphaTimerEntities();

        // GET: Taches
        public ActionResult Index()
        {
            var tasks = db.Tasks.Include(t => t.Ressources).Include(t => t.Sponsor).Include(t => t.Stories);
            return View(tasks.ToList());
        }

        [ChildActionOnly]
        public ActionResult _Index()
        {
            var tasks = db.Tasks.ToList();
            return PartialView("_Index", tasks);
        }

        [ChildActionOnly]
        public ActionResult _DayTach()
        {
            var daytach = db.Tasks.ToList();
            return PartialView("_DayTach", daytach);
        }

        [ChildActionOnly]
        public ActionResult _Daywork()
        {
            var daywork = db.Tasks.ToList();
            return PartialView("_Daywork", daywork);
        }

        // GET: Taches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            return View(tasks);
        }

        // GET: Taches/Create
        public ActionResult Create()
        {
            ViewBag.Id_res_task = new SelectList(db.Ressources, "Id_res", "Nom_res");
            ViewBag.Id_spon_task = new SelectList(db.Sponsor, "Id_spon", "Desc_spon");
            ViewBag.Id_st_task = new SelectList(db.Stories, "Id_st", "Nom_st");
            return View();
        }

        // POST: Taches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_task,Id_st_task,Nom_task,Desc_task,Id_res_task,D_D_est_task,D_F_est_task,Travail_est_task,D_D_task,D_F_task,Travail_task,Id_spon_task,Valide_le_task,Actif_task")] Tasks tasks)
        {
            if (ModelState.IsValid)
            {
                db.Tasks.Add(tasks);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_res_task = new SelectList(db.Ressources, "Id_res", "Nom_res", tasks.Id_res_task);
            ViewBag.Id_spon_task = new SelectList(db.Sponsor, "Id_spon", "Desc_spon", tasks.Id_spon_task);
            ViewBag.Id_st_task = new SelectList(db.Stories, "Id_st", "Nom_st", tasks.Id_st_task);
            return View(tasks);
        }

        // GET: Taches/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_res_task = new SelectList(db.Ressources, "Id_res", "Nom_res", tasks.Id_res_task);
            ViewBag.Id_spon_task = new SelectList(db.Sponsor, "Id_spon", "Desc_spon", tasks.Id_spon_task);
            ViewBag.Id_st_task = new SelectList(db.Stories, "Id_st", "Nom_st", tasks.Id_st_task);
            return View(tasks);
        }

        // POST: Taches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_task,Id_st_task,Nom_task,Desc_task,Id_res_task,D_D_est_task,D_F_est_task,Travail_est_task,D_D_task,D_F_task,Travail_task,Id_spon_task,Valide_le_task,Actif_task")] Tasks tasks)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tasks).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_res_task = new SelectList(db.Ressources, "Id_res", "Nom_res", tasks.Id_res_task);
            ViewBag.Id_spon_task = new SelectList(db.Sponsor, "Id_spon", "Desc_spon", tasks.Id_spon_task);
            ViewBag.Id_st_task = new SelectList(db.Stories, "Id_st", "Nom_st", tasks.Id_st_task);
            return View(tasks);
        }

        // GET: Taches/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tasks tasks = db.Tasks.Find(id);
            if (tasks == null)
            {
                return HttpNotFound();
            }
            return View(tasks);
        }

        // POST: Taches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tasks tasks = db.Tasks.Find(id);
            db.Tasks.Remove(tasks);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
