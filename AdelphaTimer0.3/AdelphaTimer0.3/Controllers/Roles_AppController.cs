﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdelphaTimer0._3.Models;

namespace AdelphaTimer0._3.Controllers
{
    public class Roles_AppController : Controller
    {
        private adelphaTimerEntities db = new adelphaTimerEntities();

        // GET: Roles_App
        public ActionResult Index()
        {
            return View(db.Roles_App.ToList());
        }

        [ChildActionOnly]
        public ActionResult _Index()
        {
            var rolesUers = db.Roles_App.ToList();
            return PartialView("_Index", rolesUers);
        }

        // GET: Roles_App/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles_App roles_App = db.Roles_App.Find(id);
            if (roles_App == null)
            {
                return HttpNotFound();
            }
            return View(roles_App);
        }

        // GET: Roles_App/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles_App/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_role_app,Desc_role_app")] Roles_App roles_App)
        {
            if (ModelState.IsValid)
            {
                db.Roles_App.Add(roles_App);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(roles_App);
        }

        // GET: Roles_App/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles_App roles_App = db.Roles_App.Find(id);
            if (roles_App == null)
            {
                return HttpNotFound();
            }
            return View(roles_App);
        }

        // POST: Roles_App/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_role_app,Desc_role_app")] Roles_App roles_App)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roles_App).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roles_App);
        }

        // GET: Roles_App/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roles_App roles_App = db.Roles_App.Find(id);
            if (roles_App == null)
            {
                return HttpNotFound();
            }
            return View(roles_App);
        }

        // POST: Roles_App/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Roles_App roles_App = db.Roles_App.Find(id);
            db.Roles_App.Remove(roles_App);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
