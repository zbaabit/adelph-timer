﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdelphaTimer0._3.Models;

namespace AdelphaTimer0._3.Controllers
{
    public class RessourcesController : Controller
    {
        private adelphaTimerEntities db = new adelphaTimerEntities();

        // GET: Ressources
        public ActionResult Index()
        {
            var ressources = db.Ressources.Include(r => r.Roles_App);
            return View(ressources.ToList());
        }

        [ChildActionOnly]
        public ActionResult _Index()
        {
            var ressources = db.Ressources.ToList();
            return PartialView("_Index", ressources);
        }

        // GET: Ressources/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ressources ressources = db.Ressources.Find(id);
            if (ressources == null)
            {
                return HttpNotFound();
            }
            return View(ressources);
        }

        // GET: Ressources/Create
        public ActionResult Create()
        {
            ViewBag.Id_role_app_res = new SelectList(db.Roles_App, "Id_role_app", "Desc_role_app");
            return View();
        }

        // POST: Ressources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_res,Nom_res,Prenom_res,Num_res,Login_res,Psw_res,Date_naiss_res,Date_recrut_res,Id_role_app_res,Actif_res")] Ressources ressources)
        {
            if (ModelState.IsValid)
            {
                db.Ressources.Add(ressources);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_role_app_res = new SelectList(db.Roles_App, "Id_role_app", "Desc_role_app", ressources.Id_role_app_res);
            return View(ressources);
        }

        // GET: Ressources/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ressources ressources = db.Ressources.Find(id);
            if (ressources == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_role_app_res = new SelectList(db.Roles_App, "Id_role_app", "Desc_role_app", ressources.Id_role_app_res);
            return View(ressources);
        }

        // POST: Ressources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_res,Nom_res,Prenom_res,Num_res,Login_res,Psw_res,Date_naiss_res,Date_recrut_res,Id_role_app_res,Actif_res")] Ressources ressources)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ressources).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_role_app_res = new SelectList(db.Roles_App, "Id_role_app", "Desc_role_app", ressources.Id_role_app_res);
            return View(ressources);
        }

        // GET: Ressources/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ressources ressources = db.Ressources.Find(id);
            if (ressources == null)
            {
                return HttpNotFound();
            }
            return View(ressources);
        }

        // POST: Ressources/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ressources ressources = db.Ressources.Find(id);
            db.Ressources.Remove(ressources);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
