﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdelphaTimer0._3.Models;

namespace AdelphaTimer0._3.Controllers
{
    public class SprintsController : Controller
    {
        private adelphaTimerEntities db = new adelphaTimerEntities();

        // GET: Sprints
        public ActionResult Index()
        {
            var sprints = db.Sprints.Include(s => s.Projects).Include(s => s.Ressources);
            return View(sprints.ToList());
        }

        [ChildActionOnly]
        public ActionResult _Index()
        {
            var sprint = db.Sprints.ToList();
            return PartialView("_Index", sprint);
        }

        // GET: Sprints/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            return View(sprints);
        }

        // GET: Sprints/Create
        public ActionResult Create()
        {
            ViewBag.Id_p_sprint = new SelectList(db.Projects, "Id_proj", "Nom_proj");
            ViewBag.Id_res_sprint = new SelectList(db.Ressources, "Id_res", "Nom_res");
            return View();
        }

        // POST: Sprints/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_sprint,Id_p_sprint,Id_res_sprint,Date_deb_sprint,Date_fin_sprint,Etat_sprint,Actif_sprint")] Sprints sprints)
        {
            if (ModelState.IsValid)
            {
                db.Sprints.Add(sprints);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_p_sprint = new SelectList(db.Projects, "Id_proj", "Nom_proj", sprints.Id_p_sprint);
            ViewBag.Id_res_sprint = new SelectList(db.Ressources, "Id_res", "Nom_res", sprints.Id_res_sprint);
            return View(sprints);
        }

        // GET: Sprints/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_p_sprint = new SelectList(db.Projects, "Id_proj", "Nom_proj", sprints.Id_p_sprint);
            ViewBag.Id_res_sprint = new SelectList(db.Ressources, "Id_res", "Nom_res", sprints.Id_res_sprint);
            return View(sprints);
        }

        // POST: Sprints/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_sprint,Id_p_sprint,Id_res_sprint,Date_deb_sprint,Date_fin_sprint,Etat_sprint,Actif_sprint")] Sprints sprints)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sprints).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_p_sprint = new SelectList(db.Projects, "Id_proj", "Nom_proj", sprints.Id_p_sprint);
            ViewBag.Id_res_sprint = new SelectList(db.Ressources, "Id_res", "Nom_res", sprints.Id_res_sprint);
            return View(sprints);
        }

        // GET: Sprints/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sprints sprints = db.Sprints.Find(id);
            if (sprints == null)
            {
                return HttpNotFound();
            }
            return View(sprints);
        }

        // POST: Sprints/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sprints sprints = db.Sprints.Find(id);
            db.Sprints.Remove(sprints);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
